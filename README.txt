Teaser Modifier module for Drupal 6

Developed by SergeChel <sergechel@live.ru>
Website: http://www.sergechel.info

A small module to modify node teasers. It removes anchor tags
from node teaser, or replaces URLs with node path, or replaces
anchor tags with span tags with specified CSS class.

Sometimes the beginning of node text contains internal links to
other pages or outgoing links to other websites, so they appear
in automatic node teasers, making taxonomy lists full of anchor
tags, which is bad for SEO. Sometimes visitors are clicking these
links in teasers and not even visiting your pages.

This small and simple module gives you some ways to deal with
this situation:

- Strip all <a> tags from node teasers
- Replace URIes in teaser with node URI
- Replace <a> tags with <span> tags, with specified CSS class name

After installation of this module go to the Site configuration –
Teaser Moduifier and choose the preferred option.
