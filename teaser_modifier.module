<?php

/**
 * @file
 * Drupal 6 node teaser modifier.
 */

/**
 * Implements hook_help().
 */
function teaser_modifier_help($path, $arg) {
  $output = '';
  switch ($path) {
    case "admin/help#teaser_modifier":
      $output = '<p>' . t("A small module to modify node teasers") . '</p>';
      break;
  }
  return $output;
}

/**
 * Implements hook_perm().
 */
function teaser_modifier_perm() {
  return array('administer teaser modifier settings');
}

/**
 * Implements hook_menu().
 */
function teaser_modifier_menu() {
  $items = array();

  $items['admin/settings/teaser_modifier'] = array(
    'title' => 'Teaser modifier',
    'description' => 'Teaser modifier module settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('teaser_modifier_admin'),
    'access arguments' => array('administer teaser modifier settings'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Build settings webform.
 */
function teaser_modifier_admin() {

  if (!user_access('administer teaser modifier settings')) {
    drupal_set_message(t('You need a role permission to edit these settings'), 'error');
    return NULL;
  }

  $form = array();

  $form['teaser_modify_action_type'] = array(
    '#type' => 'radios',
    '#title' => t('Action type'),
    '#default_value' => variable_get('teaser_modify_action_type', 0),
    '#options' => array(
      t('Do nothing'),
      t('Strip &lt;a&gt; tags from teaser'),
      t('Replace URIes in teaser with node URI'),
      t('Replace &lt;a&gt; tag with &lt;span&gt; tag, with specified CSS class name'),
    ),
  );

  $form['teaser_modify_css_class'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS class name'),
    '#default_value' => variable_get('teaser_modify_css_class', ''),
    '#size' => 30,
    '#maxlength' => 64,
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Validate submitted settings.
 */
function teaser_modifier_admin_validate($form, &$form_state) {
  $action_type = $form_state['values']['teaser_modify_action_type'];
  $css_class = $form_state['values']['teaser_modify_css_class'];
  if (!is_numeric($action_type) || $action_type > 3) {
    form_set_error('teaser_modify_action_type', t('Wrong action type value has been submitted.'));
  }
  elseif ($action_type == 3 && strlen($css_class) > 64) {
    form_set_error('teaser_modify_css_class', t('CSS class name too long.'));
  }
}

/**
 * Implements hook_nodeapi().
 */
function teaser_modifier_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  // Get action type.
  $action_type = variable_get('teaser_modify_action_type', 0);

  // Modify rendered node body, $a3 is a teaser flag in this case.
  if ($op == 'alter' && $a3) {
    // Get rendered body content.
    $body = $node->content['body']['#value'];

    switch ($action_type) {

      case 1:
        // Remove anchor tags.
        $body = preg_replace('#</?a[^>]*>#is', '', $body);
        break;

      case 2:
        // Replace urls with node url or path alias.
        $body = preg_replace('#(<a.*?>)#', '<a>', $body);
        $path = drupal_get_path_alias($node->links['node_read_more']['href']);
        $body = str_replace('<a>', '<a href="' . $path . '">', $body);
        break;

      case 3:
        // Replace anchor tags with span tags with specified CSS class.
        $css_class = variable_get('teaser_modify_css_class', '');
        if (!empty($css_class)) {
          $css_class = ' class="' . $css_class . '"';
        }
        $body = preg_replace('#(<a.*?>)#', '<a>', $body);
        $body = str_replace('<a>', '<span' . $css_class . '>', $body);
        $body = str_replace('</a>', '</span>', $body);
        break;

      default:
        break;
    }
    // Update body content.
    $node->content['body']['#value'] = $body;
  }
}
